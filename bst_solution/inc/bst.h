#ifndef BST_H
#define BST_H

struct node{
    unsigned long key;    // src ip + port
    unsigned long val;    // dst ip + port
    struct node* left, *right;
};

struct node* insert(struct node* node, unsigned long k, unsigned long v);
struct node* search(struct node* root, unsigned long k);

#ifdef DEBUG
void inorder(struct node* root);
#endif

#endif
