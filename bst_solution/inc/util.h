#ifndef UTIL_H
#define UTIL_H

#define MAX_IP_LEN   (32 + 1)
#define MAX_PORT_LEN (5 + 1)

unsigned long ip_port2num(unsigned int ip, unsigned int port);
unsigned int ip2num(const char* ip);
unsigned int port2num(const char* port);
int num2ip(unsigned long num, char* ip, int len);
int num2port(unsigned long num, char* port, int len);
int nat2num(char* line, unsigned long* src, unsigned long* dst);
int flow2num(char* line, unsigned long* ret);

#endif
