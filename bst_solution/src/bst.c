#include "bst.h"
#include <stdlib.h>
#include <stdio.h>

/* **************************************************************** *
 * @k [IN]: key by source IP[47:16] + source port[15:0]             *
 * @v [IN]: value by destination IP[47:16] + destination port[15:0] *
 *                                                                  *
 * @ If not enough memory, return NULL; otherwise, return the node  *
 * **************************************************************** */

static struct node* create_node(unsigned long k, unsigned long v){
    struct node* tmp = (struct node *)malloc(sizeof(struct node));
    if(tmp == NULL){
        printf("Not enough memory. Malloc fail.\n");
        return NULL;
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    tmp->key = k;
    tmp->val = v;
    tmp->left = NULL;
    tmp->right = NULL;

    return tmp;
}

/* ****************************************************************** *
 * insert into a Binary Search Tree                                   *
 * @node[IN]: node of the root                                        *
 * @k[IN]   : key by source IP[47:16] + source port[15:0]             *
 * @v[IN]   : value by destination IP[47:16] + destination port[15:0] *
 *                                                                    *
 * @ return the root of the BST                                       *
 * ****************************************************************** */

struct node* insert(struct node* node, unsigned long k, unsigned long v){
    if(node == NULL){
        return create_node(k, v);
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    if(k < node->key){
        node->left = insert(node->left, k, v);
    }else if(k > node->key){
        node->right = insert(node->right, k, v);
    }else{
        printf("the source ip/port pair already exist\n");
    }

    return node;
}

/* *********************************************************************************** *
 * traverse the tree in-order. It is expected to see the ascending order in the output.*
 * @root[IN]: root of the BST
 * *********************************************************************************** */

#ifdef DEBUG
void inorder(struct node* root){
    if(root == NULL){
        return;
    }

    if(root != NULL){
        inorder(root->left);
        printf("key:%lx, val:%lx\n", root->key, root->val);
        inorder(root->right);
    }
}
#endif

/* ***************************************************************** *
 * Search the k in BST tree                                          *
 * @k [IN]: key by source IP[47:16] + source port[15:0]              *
 * @return the node if there is the key match; otherwise return NULL *
 * ***************************************************************** */

struct node* search(struct node* root, unsigned long k){
    if(root == NULL){
        printf("This is a null bst.\n");
        return NULL;
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    struct node* curr = root;

    while(curr != NULL){
        if(k < curr->key){
            curr = curr->left;
        }else if(k > curr->key){
            curr = curr->right;
        }else{
            return curr;
        }
    }

    return curr;
}
