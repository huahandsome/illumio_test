#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>

#define MAX_IP_LEN   (32 + 1)
#define MAX_PORT_LEN (5 + 1)

/* ************************************************* *
 * @ip[IN]   :IP address with HEX representation     *
 * @port[IN] :Port number                            *
 *                                                   *
 * @ return long ineger: the IP[47:16] | port [15:0] *
 * ************************************************* */

unsigned long ip_port2num(unsigned int ip, unsigned int port){
    unsigned long ret = 0;

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    ret = ip;
    ret = (ret << 16) | port;

    return ret;
}

/* **************************************** *
 * @ip[IN] : Input an IP with string        *
 * @ return the number representation of IP *
 * **************************************** */

unsigned int ip2num(const char* ip){
    if(ip == NULL){
        printf("Error. Null pointer\n");
        return -1;
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    unsigned int ret = 0;

    if(ip[0] != '*'){
        unsigned int tmp = inet_addr(ip);

        ret = ((tmp & 0xff000000) >> 24) | \
              ((tmp & 0x00ff0000) >> 8 ) | \
              ((tmp & 0x0000ff00) << 8 ) | \
              ((tmp & 0x000000ff) << 24);
    }

    return ret;
}

/* ****************************************** *
 * @ip[IN] : Input an port with string        *
 * @ return the number representation of port *
 * ****************************************** */

unsigned int port2num(const char* port){
    if(port == NULL){
        printf("Error. Null pointer\n");
        return -1;
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    unsigned int ret = 0;
    if(port[0] != '*'){
        ret = atoi(port);
    }

    return ret;
}

/* *************************************************** *
 * Given a number (IP+port), parse the IP              *
 * @num[IN]                                            *
 * @ip[OUT]: provide the buffer to store the ip string *
 * @len[IN]: length of the buffer                      *
 *                                                     *
 * return 0 if success; otherwise return -1            *
 * *************************************************** */

int num2ip(unsigned long num, char* ip, int len){
    if(ip == NULL){
        printf("Error. Null pointer\n");
        return -1;
    }

    if(len < MAX_IP_LEN){
        printf("Error. Len for ip is too small\n");
        return -1;
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif
    memset(ip, 0, len);
    unsigned int ip_addr = (num & 0xFFFFFFFF0000) >> 16;

    unsigned int a = (ip_addr & 0xFF000000) >> 24;
    unsigned int b = (ip_addr & 0x00FF0000) >> 16;
    unsigned int c = (ip_addr & 0x0000FF00) >> 8;
    unsigned int d = (ip_addr & 0x000000FF);

    snprintf(ip, len, "%u.%u.%u.%u", a, b, c, d);

    return 0;
}

/* ******************************************************** *
 * Given a number (IP+port), parse the Port                 *
 * @num[IN]                                                 *
 * @port[OUT]: provide the buffer to store the port string  *
 * @len[IN]: length of the buffer                           *
 *                                                          *
 * return 0 if success; otherwise return -1                 *
 * ******************************************************** */

int num2port(unsigned long num, char* port, int len){
    if(port == NULL){
        printf("Error. Null pointer\n");
        return -1;
    }

    if(len < MAX_PORT_LEN){
        printf("Error. Len for port is too small\n");
        return -1;
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    memset(port, 0, len);
    unsigned int port_num = (num & 0xFFFF);

    snprintf(port, len, "%d", port_num);

    return 0;
}

/* ********************************************************************** *
 * given a line reading from NAT file, output src(ip+port) & dst(ip+port) *
 * @line[IN] : 192.168.1.2:234,3.4.5.6:123                                *
 * @src [OUT]: output source IP [47:16] + source port[15:0]               *
 * @dst [OUT]: output destination IP [47:16] + destination port[15:0]     *
 * @return 0 if success; otherwise return -1.                             *
 * ********************************************************************** */

int nat2num(char* line, unsigned long* src, unsigned long* dst){
    if(line == NULL || src == NULL || dst == NULL){
        printf("Error. Null pointer\n");
        return -1;
    }
#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    char* start = line;
    char* end = strchr(line, ',');
    if(end == NULL){
        printf("Error. It is expected , to split src and dst");
        return -1;
    }

    *end = '\0';
    char* sep = strchr(start, ':');
    *sep = '\0';

    unsigned int src_ip = ip2num(start);
    if(src_ip == -1){
        printf("There is an error to convert src ip to num.\n");
        return -1;
    }

    unsigned int src_port = port2num(sep+1);
    if(src_port == -1){
        printf("There is an error to convert src port to num.\n");
        return -1;
    }

    start = end+1;
    sep = strchr(start, ':');
    *sep = '\0';

    unsigned int dst_ip = ip2num(start);
    if(dst_ip == -1){
        printf("There is an error to convert dst ip to num.\n");
        return -1;
    }

    unsigned int dst_port = port2num(sep+1);
    if(dst_port == -1){
        printf("There is an error to convert dst port to num.\n");
        return -1;
    }

#ifdef DEBUG
    printf("src ip: %x, src_port: %d, dst_ip: %x, dst_port: %d\n", src_ip, src_port, dst_ip, dst_port);
#endif

    *src = ip_port2num(src_ip, src_port);
    *dst = ip_port2num(dst_ip, dst_port);

    return 0;
}

/* ******************************************************** *
 * given a line reading from FLOW file, output ret(ip+por)  *
 * @line[IN] : 192.168.1.2:234                              *
 * @ret [OUT]: output source IP [47:16] + source port[15:0] *
 * @return 0 if success; otherwise return -1.               *
 * ******************************************************** */

int flow2num(char* line, unsigned long* ret){
    if(line == NULL || ret == NULL){
        printf("Error. Null pointer\n");
        return -1;
    }

#ifdef DEBUG
    printf("Enter Function: %s.\n", __FUNCTION__);
#endif

    char* start = line;
    char* sep = strchr(line, ':');
    *sep = '\0';

    unsigned int ip = ip2num(start);
    unsigned int port = port2num(sep+1);

    *ret = ip_port2num(ip, port);

    return 0;
}
