#### how you tested your solution

#### any interesting coding, design, or algorithmic choices you�d like to point out
    
    1. the naive thinking for this problem is to read NAT file into a link list, then read the FLOW file and seach in the link list to match.
    2. if use the list data structure, the time complexitiy is not good, is will be slow especially when the NAT table becomes large. O(M*N);
    3. I chose the BST, which optimized the time complexity to O(M*log(N));
    4. I will provide a documentation of software design document (under *doc* of this repo), in which I will show details how I think this problem, finally how I implemented it.
    
#### any refinements or optimizations that you would�ve implemented if you had more time
    I am still trying to optimize this by using hash table. We can face to face discuss if I am luck to have the interview.
    
#### how you modified or made the challenge more well defined if you did
    Currently, this requirement of this task is clear. I do not think I have any comments on it.
    
#### anything else you�d like the reviewer to know
    I am not sure if I should use C++ or JAVA to solve this task or not. Since I am confident on my C programming, I choose C. 
    However, at this time, I am familiar with Python and Java as well.
    
#### about how to use
    1. I put all source code into *src* folder, while the header files are located in *inc* folder. 
    2. There is a *Makefile* in *src* folder, run *make* under the folder will generate the binary file, named "app"
    3. by default, it will not compile with log information. You can easily enable log by mofifying the Makefile.
    4. I put the *NAT* and *FLOW* file into *data_in*; while the output file will be stored in *data_out* by default.
    