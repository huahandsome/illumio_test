#ifndef HASH_H
#define HASH_H

struct node{
    unsigned long key;
    unsigned long val;
    struct node* next;
};

struct table{
    int size;
    struct node** list;
};

int create_table(int size, struct table* table);
int hash_code(unsigned long number);
int insert(struct table t, unsigned long k, unsigned long v);
struct node* search(struct table t, unsigned long key);
void print_table(int size, struct table t);
#endif
