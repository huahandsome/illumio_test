#include <stdio.h>
#include <string.h>
#include "util.h"
#include "hash.h"

#define NAT_FILENAME  "../data_in/nat.txt"
#define FLOW_FILENAME "../data_in/flow.txt"
#define OUT_FILENAME  "../data_out/out.txt"

#define MAX_LINE_LEN  256
#define MAX_OUT_LEN   256

int main(){
    FILE* file = fopen(NAT_FILENAME, "r");
    if(file == NULL){
        printf("Error. Fail to open the file: %s\n", NAT_FILENAME);
        return 0;
    }
    char line[MAX_LINE_LEN] = {0};
    struct table hash_table = {0};
    create_table(10, &hash_table);

    unsigned long src = 0, dst = 0;
    while(fgets(line, sizeof(line), file)){    // read the NAT file to mem, and build BST tree
#ifdef DEBUG
        printf("read NAT file: %s", line);
#endif
        nat2num(line, &src, &dst);

#ifdef DEBUG
        printf("src(ip+port): %lx, dst(ip+port): %lx\n", src, dst);
#endif
        insert(hash_table, src, dst);
    }
    print_table(10, hash_table);
    fclose(file);

    file = fopen(FLOW_FILENAME, "r");
    if(file == NULL){
        printf("Error. Fail to open file: %s\n", FLOW_FILENAME);
        return 0;
    }

    FILE* out_file = fopen(OUT_FILENAME, "w+");
    if(out_file == NULL){
        printf("Error. Fail to open file: %s\n", OUT_FILENAME);
        return 0;
    }

    memset(line, 0, MAX_LINE_LEN);
    unsigned long src_ip_port = 0;
    char src_ip[MAX_IP_LEN] = {0};
    char src_port[MAX_PORT_LEN] = {0};
    char dst_ip[MAX_IP_LEN] = {0};
    char dst_port[MAX_PORT_LEN] = {0};
    char output[MAX_OUT_LEN] = {0};
    char tmp[MAX_LINE_LEN] = {0};

    while(fgets(line, sizeof(line), file)){
#ifdef DEBUG
        printf("read flow line:%s", line);
#endif
        memset(src_ip, 0, MAX_IP_LEN);
        memset(src_port, 0, MAX_PORT_LEN);
        memset(dst_ip, 0, MAX_IP_LEN);
        memset(dst_port, 0, MAX_PORT_LEN);
        memset(output, 0, MAX_OUT_LEN);
        memset(tmp, 0, MAX_IP_LEN + MAX_PORT_LEN + 1);

        snprintf(tmp, strlen(line), "%s", line);
#ifdef DEBUG
        printf("copy flow line:%s, %d", tmp, (int)strlen(line));
#endif

        flow2num(line, &src_ip_port);
        num2ip(src_ip_port, src_ip, MAX_IP_LEN);
        num2port(src_ip_port, src_port, MAX_PORT_LEN);

#ifdef DEBUG
        printf("src(ip+port): %lx, src ip: %s, src port: %s\n", src_ip_port, src_ip, src_port);
#endif
        struct node* node = search(hash_table, src_ip_port);    // ip:port match

        if(node == NULL){
            node = search(hash_table, src_ip_port & 0xFFFF);    // *:port match
            if(node != NULL){
                num2ip(node->val, dst_ip, MAX_IP_LEN);
                num2port(node->val, dst_port, MAX_PORT_LEN);
                snprintf(output, MAX_OUT_LEN, "%s -> %s:%s\n", tmp, dst_ip, dst_port);
                fwrite(output, 1, strlen(output), out_file);
#ifdef DEBUG
                printf("output is %s\n", output);
#endif
            }else{
                node = search(hash_table, (src_ip_port & 0xFFFFFFFF0000)); // ip:* match
                if(node != NULL){
                    num2ip(node->val, dst_ip, MAX_IP_LEN);
                    num2port(node->val, dst_port, MAX_PORT_LEN);
                    snprintf(output, MAX_OUT_LEN, "%s -> %s:%s\n", tmp, dst_ip, dst_port);
                    fwrite(output, 1, strlen(output), out_file);
#ifdef DEBUG
                    printf("output is %s\n", output);
#endif
                }else{
                    snprintf(output, MAX_OUT_LEN, "%s -> %s\n", "No nat match for", tmp);
                    fwrite(output, 1, strlen(output), out_file);
#ifdef DEBUG
                    printf("output is %s\n", output);
#endif
                }
            }
        }else{
            num2ip(node->val, dst_ip, MAX_IP_LEN);
            num2port(node->val, dst_port, MAX_PORT_LEN);
            snprintf(output, MAX_OUT_LEN, "%s -> %s:%s\n", tmp, dst_ip, dst_port);
            fwrite(output, 1, strlen(output), out_file);
#ifdef DEBUG
            printf("output is %s\n", output);
#endif
        }
    }

    fclose(file);
    fclose(out_file);

    return 0;
}
