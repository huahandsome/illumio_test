#include "hash.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int create_table(int size, struct table* table){
    if(table == NULL){
        printf("There is null pointer\n");
        return -1;
    }

    table->list = (struct node**)malloc(size*sizeof(struct node));

    if(table->list == NULL){
        printf("There is not enough memory\n");
        return -1;
    }

    table->size = size;
    for(int i = 0; i < size; i++){
        table->list[i] = NULL;
    }

    return 0;
}

int hash_code(unsigned long number){
    return (number % 10);
}

int insert(struct table t, unsigned long k, unsigned long v){
    int h_code = hash_code(k);

    struct node* head = t.list[h_code];

    struct node* new_node = (struct node*)malloc(sizeof(struct node));

    if(new_node == NULL){
        printf("This is not enough memory\n");
        return -1;
    }

    new_node->key = k;
    new_node->val = v;
    new_node->next = NULL;

    if(head == NULL){
        t.list[h_code] = new_node;
    }else{
        struct node* tmp = head;

        while(tmp->next != NULL){
            tmp = tmp->next;
        }

        tmp->next = new_node;
    }

    return 0;
}

struct node* search(struct table t, unsigned long key){
    int h_code = hash_code(key);

    struct node* list = t.list[h_code];
    struct node* ret = NULL;

    if(list == NULL){
        return ret;
    }else{
        struct node* head = list;

        while(head != NULL){
            if(head->key == key){
                ret = head;
                break;
            }
            head = head->next;
        }
    }

    return ret;
}

void print_table(int size, struct table t){
    for(int i = 0; i < size; i++){
        struct node* head = t.list[i];

        if(head != NULL){
            printf("--> %d:\n", i);

            while(head != NULL){
                printf("key: %lx, val: %lx\n", head->key, head->val);
                head = head->next;
            }
        }else{
            printf("--> %d is null list\n", i);
        }
    }
}
